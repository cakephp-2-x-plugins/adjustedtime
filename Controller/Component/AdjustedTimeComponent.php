<?php
App::uses('Component', 'Controller');
App::uses('AdjustedTime', 'AdjustedTime.Lib');

/**
 * Handles setting user timezone information from session.
 *
 * @author Jason Burgess <jason@notplugged.in>
 * @version 1.1
 */
class AdjustedTimeComponent extends Component {
	public $components = array('Session');

	/**
	 * (non-PHPdoc)
	 *
	 * @see Component::startup()
	 * @since 1.0
	 */
	public function startup(Controller $controller) {
		// Tell it to use the cache if it exists.
		if (class_exists('ZendPlugin') && ZendPlugin::$cache) {
			Zend_Date::setOptions(array('cache' => ZendPlugin::$cache));
		}

		AdjustedTime::$userLocale = new Zend_Locale();

		// Get the server/default timezone
		$server_tz = Configure::read('AdjustedTime.Timezone');
		if (!$server_tz) {
			$server_tz = date_default_timezone_get();
		}

		if ($server_tz) {
			AdjustedTime::$default = $server_tz;
		}

		// Set the user timezone variables
		$tz = $this->Session->read('TZ');
		if ($tz) {
			$controller->request->params['tz_detected'] = $tz;
			try {
				if (!empty($tz['name'])) {
					AdjustedTime::$userTimezone = new DateTimeZone($tz['name']);
				} else {
					AdjustedTime::$userTimezone = new DateTimeZone(AdjustedTime::$default);
				}
			} catch (Exception $e) {
				AdjustedTime::$userTimezone = new DateTimeZone(AdjustedTime::$default);
			}
		}
	}
}