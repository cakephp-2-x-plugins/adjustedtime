<?php
App::uses('AppController', 'Controller');
App::uses('InputFilter', 'InputFilter.Lib');

/**
 * TimezoneController for detecting user's timezone automatically.
 *
 * @author Jason Burgess <jason@notplugged.in>
 * @version 1.1
 */
class TimezoneController extends AppController {
	public $helpers = array('Form', 'Time' => array('className' => 'AdjustedTime.AdjustedTime'));

	/**
	 * (non-PHPdoc)
	 *
	 * @see AppController::beforeFilter()
	 */
	function beforeFilter() {
		// In case Auth is implemented.
		if (!empty($this->Auth)) {
			$this->Auth->allow();
		}
	}

	/**
	 * Send actual image to user, and pull information about their timezone.
	 *
	 * @param number $offset
	 *        	Offset from UTC
	 * @param number $dst
	 *        	Is DST
	 */
	function image($offset = 0, $dst = 0) {
		// Give them an empty pixel.
		$path = APP . 'Plugin/AdjustedTime/webroot/img/pixel.gif';
		$this->response->disableCache();
		$this->response->file($path,
				array(
						'download' => false,
						'name' => sprintf('timezone%d%d', $offset, $dst),
						'extension' => 'gif'));

		// Grab the information about their timezone
		$tz = array('offset' => InputFilter::int($offset), 'dst' => InputFilter::int($dst));

		if (empty($tz['offset'])) {
			$tz['offset'] = 0;
		}

		if (empty($tz['dst'])) {
			$tz['dst'] = 0;
		}

		// Get the closest available timezone.
		$tz['name'] = timezone_name_from_abbr('', -$tz['offset'] * 60, $tz['dst']);

		// Save the timezone information to the session.
		$this->Session->write('TZ', $tz);

		// Disable debugging to keep it from interferring on display.
		Configure::write('debug', 0);

		return $this->response;
	}
}
