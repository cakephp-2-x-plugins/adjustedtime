<?php
if (empty($this->request->params['tz_detected'])) {
?>
<script type="text/javascript">
	(function() {
		"use strict";
		var now, d1, d2, dst = 0, offset, hemisphere, ft, nowOffset, d1Offset, d2Offset, img;
		now = d1 = d2 = new Date();
		offset = now.getTimezoneOffset();

		d1.setDate(1);
		d1.setMonth(1);

		d2.setDate(1);
		d2.setMonth(7);

		d1Offset = parseInt(d1.getTimezoneOffset(), 10);
		d2Offset = parseInt(d1.getTimezoneOffset(), 10);

		if (d1Offset !== d2Offset) {
			hemisphere = d1Offset - d2Offset;
			nowOffset = parseInt(now.getTimezoneOffset(), 10);
			if (!((hemisphere > 0 && d1Offset === nowOffset) || (hemisphere < 0 && d2Offset === nowOffset))) {
				dst = 1;
			}
		}

		if (document.createElement) {
			img = document.createElement('img');
			img.style = 'display: none';
			img.src = '/adjusted_time/timezone/image/' + offset.toString() + '/' + dst.toString();
		}
	}());
</script>
<?php
}
?>