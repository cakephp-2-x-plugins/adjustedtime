<?php
App::uses('TimeHelper', 'View/Helper');
App::uses('AdjustedTime', 'AdjustedTime.Lib');

/**
 * Helper wrapper for AdjustedTime library.
 *
 * @author Jason Burgess <jason@notplugged.in>
 * @version 1.1
 */
class AdjustedTimeHelper extends TimeHelper {
	/**
	 * Create an instance of AdjustedTimeHelper
	 *
	 * @param View $View
	 * @param unknown $settings
	 * @since 1.0
	 */
	public function __construct(View $View, $settings = array()) {
		// Have to override this much to change the engine.
		$settings = Set::merge(array('engine' => 'AdjustedTime'), $settings);
		parent::__construct($View, $settings);
	}

	/**
	 *
	 * @see AdjustedTime::convert()
	 *
	 * @param string $serverTime
	 *        	UNIX timestamp
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return integer UNIX timestamp
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#formatting
	 * @since 1.0
	 */
	public function adjust($serverTime, $userTime = true) {
		return $this->_engine->adjust($serverTime, $userTime);
	}

	/**
	 *
	 * @see AdjustedTime::fromString()
	 *
	 * @param string $dateString
	 *        	Datetime string
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return string Parsed timestamp
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#formatting
	 * @since 1.0
	 */
	public function fromString($dateString, $userTime = true) {
		return $this->_engine->fromString($dateString, $userTime);
	}

	/**
	 *
	 * @see AdjustedTime::nice()
	 *
	 * @param string $dateString
	 *        	Datetime string or Unix timestamp
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @param string $format
	 *        	The format to use. If null, `TimeHelper::$niceFormat` is used
	 * @return string Formatted date string
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#formatting
	 * @since 1.0
	 */
	public function nice($dateString = null, $userTime = true, $format = null) {
		return $this->_engine->nice($dateString, $userTime, $format);
	}

	/**
	 *
	 * @see AdjustedTime::niceShort()
	 *
	 * @param string $dateString
	 *        	Datetime string or Unix timestamp
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return string Described, relative date string
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#formatting
	 * @since 1.0
	 */
	public function niceShort($dateString = null, $userTime = true) {
		return $this->_engine->niceShort($dateString, $userTime);
	}

	/**
	 *
	 * @see AdjustedTime::daysAsSql()
	 *
	 * @param string $begin
	 *        	Datetime string or Unix timestamp
	 * @param string $end
	 *        	Datetime string or Unix timestamp
	 * @param string $fieldName
	 *        	Name of database field to compare with
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return string Partial SQL string.
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#formatting
	 * @since 1.0
	 */
	public function daysAsSql($begin, $end, $fieldName, $userTime = true) {
		return $this->_engine->daysAsSql($begin, $end, $fieldName, $userTime);
	}

	/**
	 *
	 * @see AdjustedTime::dayAsSql()
	 *
	 * @param string $dateString
	 *        	Datetime string or Unix timestamp
	 * @param string $fieldName
	 *        	Name of database field to compare with
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return string Partial SQL string.
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#formatting
	 * @since 1.0
	 */
	public function dayAsSql($dateString, $fieldName, $userTime = true) {
		return $this->_engine->dayAsSql($dateString, $fieldName, $userTime);
	}

	/**
	 *
	 * @see AdjustedTime::isToday()
	 *
	 * @param string $dateString
	 *        	Datetime string or Unix timestamp
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return boolean True if datetime string is today
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#testing-time
	 * @since 1.0
	 */
	public function isToday($dateString, $userTime = true) {
		return $this->_engine->isToday($dateString, $userTime);
	}

	/**
	 *
	 * @see AdjustedTime::isThisWeek()
	 *
	 * @param string $dateString
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return boolean True if datetime string is within current week
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#testing-time
	 * @since 1.0
	 */
	public function isThisWeek($dateString, $userTime = true) {
		return $this->_engine->isThisWeek($dateString, $userTime);
	}

	/**
	 *
	 * @see AdjustedTime::isThisMonth()
	 *
	 * @param string $dateString
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return boolean True if datetime string is within current month
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#testing-time
	 * @since 1.0
	 */
	public function isThisMonth($dateString, $userTime = true) {
		return $this->_engine->isThisMonth($dateString, $userTime);
	}

	/**
	 *
	 * @see AdjustedTime::isThisYear()
	 *
	 * @param string $dateString
	 *        	Datetime string or Unix timestamp
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return boolean True if datetime string is within current year
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#testing-time
	 * @since 1.0
	 */
	public function isThisYear($dateString, $userTime = true) {
		return $this->_engine->isThisYear($dateString, $userTime);
	}

	/**
	 *
	 * @see AdjustedTime::wasYesterday()
	 *
	 * @param string $dateString
	 *        	Datetime string or Unix timestamp
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return boolean True if datetime string was yesterday
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#testing-time
	 * @since 1.0
	 */
	public function wasYesterday($dateString, $userTime = true) {
		return $this->_engine->wasYesterday($dateString, $userTime);
	}

	/**
	 *
	 * @see AdjustedTime::isTomorrow()
	 *
	 * @param string $dateString
	 *        	Datetime string or Unix timestamp
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return boolean True if datetime string was yesterday
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#testing-time
	 * @since 1.0
	 */
	public function isTomorrow($dateString, $userTime = true) {
		return $this->_engine->isTomorrow($dateString, $userTime);
	}

	/**
	 *
	 * @see AdjustedTime::toQuarter()
	 *
	 * @param string $dateString
	 * @param boolean $range
	 *        	if true returns a range in Y-m-d format
	 * @return mixed 1, 2, 3, or 4 quarter of year or array if $range true
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#formatting
	 * @since 1.0
	 */
	public function toQuarter($dateString, $range = false) {
		return $this->_engine->toQuarter($dateString, $range);
	}

	/**
	 *
	 * @see AdjustedTime::toUnix()
	 *
	 * @param string $dateString
	 *        	Datetime string to be represented as a Unix timestamp
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return integer Unix timestamp
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#formatting
	 * @since 1.0
	 */
	public function toUnix($dateString, $userTime = true) {
		return $this->_engine->toUnix($dateString, $userTime);
	}

	/**
	 *
	 * @see AdjustedTime::toAtom()
	 *
	 * @param string $dateString
	 *        	Datetime string or Unix timestamp
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return string Formatted date string
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#formatting
	 * @since 1.0
	 */
	public function toAtom($dateString, $userTime = true) {
		return $this->_engine->toAtom($dateString, $userTime);
	}

	/**
	 *
	 * @see AdjustedTime::toRSS()
	 *
	 * @param string $dateString
	 *        	Datetime string or Unix timestamp
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return string Formatted date string
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#formatting
	 * @since 1.0
	 */
	public function toRSS($dateString, $userTime = true) {
		return $this->_engine->toRSS($dateString, $userTime);
	}

	/**
	 *
	 * @see AdjustedTime::timeAgoInWords()
	 *
	 * @param string $dateTime
	 *        	Datetime string or Unix timestamp
	 * @param array $options
	 *        	Default format if timestamp is used in $dateString
	 * @return string Relative time string.
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#formatting
	 * @since 1.0
	 */
	public function timeAgoInWords($dateTime, $options = array()) {
		return $this->_engine->timeAgoInWords($dateTime, $options);
	}

	/**
	 *
	 * @see AdjustedTime::wasWithinLast()
	 *
	 * @param mixed $timeInterval
	 *        	the numeric value with space then time type. Example of valid
	 *        	types: 6 hours, 2 days, 1 minute.
	 * @param mixed $dateString
	 *        	the datestring or unix timestamp to compare
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return boolean
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#testing-time
	 * @since 1.0
	 */
	public function wasWithinLast($timeInterval, $dateString, $userTime = true) {
		return $this->_engine->wasWithinLast($timeInterval, $dateString, $userTime);
	}

	/**
	 *
	 * @see AdjustedTime::format()
	 *
	 * @param string $format
	 *        	date format string (or a DateTime string)
	 * @param string $date
	 *        	Datetime string (or a date format string)
	 * @param boolean $invalid
	 *        	flag to ignore results of fromString == false
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return string Formatted date string
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#formatting
	 * @since 1.0
	 */
	public function format($format, $date = null, $invalid = false, $userTime = true) {
		return $this->_engine->format($format, $date, $invalid, $userTime);
	}

	/**
	 *
	 * @see AdjustedTime::i18nFormat()
	 *
	 * @param string $date
	 *        	Datetime string
	 * @param string $format
	 *        	strftime format string.
	 * @param boolean $invalid
	 *        	flag to ignore results of fromString == false
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return string Formatted and translated date string
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#formatting
	 * @since 1.0
	 */
	public function i18nFormat($date, $format = null, $invalid = false, $userTime = true) {
		return $this->_engine->i18nFormat($date, $format, $invalid, $userTime);
	}
}