AdjustedTime Plugin for CakePHP 2.x
Version 1.1

Master: [https://gitlab.com/cakephp-2-x-plugins/adjustedtime](https://gitlab.com/cakephp-2-x-plugins/adjustedtime)

# Requirements
- ZendFramework Plugin: [https://gitlab.com/cakephp-2-x-plugins/zendframework](https://gitlab.com/cakephp-2-x-plugins/zendframework)
*Using version 1.x of the Zend Framework.*

# General Information

AdjustedTime works as a drop in replacement for TimeHelper.

# Release Information

## Version 1.1
- Initial public release.
- [#4] Added class documentation.

## Usage

To enable support for AdjustedTime, you must make the following changes in your project:

In your layout file (usually *app/View/Layouts/default.ctp*), *before* the </body> tag:
``` php
<?php echo $this->element('AdjustedTime.TZImage'); ?>
```

In *app/Controller/AppController.php*, so that all your controllers will use it:
``` php
public $components = array('AdjustedTime.AdjustedTime');
public $helpers = array('Time' => array('className' => 'AdjustedTime.AdjustedTime');
```
*Note:* AdjustedTimeHelper is being referenced as TimeHelper because it includes all of the same features, with some enhancements. This way, you do not have to alter existing code that uses TimeHelper. 

In *app/Config/core.php*: **(Optional)**
``` php
Configure::write('Timezone', 'America/Chicago');
```

*Note:* This will set the server's default timezone.  Replace *America/Chicago* with your timezone. The code will attempt to automatically detect default timezone from PHP otherwise.

For a list of PHP's timezones, see [List of Supported Timezones](http://php.net/manual/en/timezones.php).

## Caching
	
AdjustedTime will automatically use Zend_Cache if the [ZendFramework plugin](https://gitlab.com/cakephp-2-x-plugins/zendframework) is loaded and ZendPlugin::$cache is initialized.

## License
This plugin is released under the BSD License.  You can find a copy of this licenese in [LICENSE.md](LICENSE.md).