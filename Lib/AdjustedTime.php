<?php
App::uses('CakeTime', 'Utility');
/**
 * Time Helper class for easy use of time data. Automatically adjusts time for
 * user's timezone. Manipulation of time data.
 *
 * @package AdjustedTime.Lib
 * @version 1.1
 */
class AdjustedTime extends CakeTime {
	/**
	 *
	 * @var string Default Timezone
	 * @since 1.0
	 */
	public static $default = 'America/Chicago';
	/**
	 *
	 * @var unknown
	 * @since 1.0
	 */
	public static $userTimezone = null;
	/**
	 *
	 * @var unknown
	 * @since 1.0
	 */
	public static $userLocale = null;

	/**
	 * The format to use when formatting a time using
	 * `AdjustedTimeHelper::nice()` The format should use the iso format
	 * specifiers.
	 *
	 * @var string
	 * @see AdjustedTimeHelper::format()
	 * @since 1.0
	 */
	public static $niceFormat = Zend_Date::DATETIME_FULL;

	/**
	 * Converts a string representing the format for the function strftime and
	 * returns a windows safe and i18n aware format.
	 *
	 * @param string $format
	 *        	Format with specifiers for strftime function. Accepts the
	 *        	special specifier %S which mimics the modifier S for date()
	 * @param string $time
	 *        	UNIX timestamp
	 * @return string windows safe and date() function compatible format for
	 *         strftime
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#formatting
	 * @since 1.0
	 */
	public static function convertSpecifiers($format, $time = null) {
		if (!$time) {
			$time = time();
		}
		self::$_time = $time;
		return Zend_Locale_Format::convertPhpToIsoFormat($format);
	}

	/**
	 * Adjust the time from one timezone to another.
	 *
	 * @param string $inTime
	 *        	input time
	 * @param boolean $toUserTime
	 *        	If false, convert from user time to server time.
	 * @return Zend_Date
	 * @since 1.0
	 */
	public static function adjust($inTime, $toUserTime = true) {
		$inTime = self::fromString($inTime, !$toUserTime);
		if (self::$userTimezone) {
			$inTime->setTimezone(self::$userTimezone->getName());
		}
		return $inTime;
	}

	/**
	 * Returns a Zend_Date object, given either a UNIX timestamp or a valid
	 * strtotime() date string.
	 *
	 * @param string $dateString
	 *        	Datetime string
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return string Parsed timestamp
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#formatting
	 * @since 1.0
	 */
	public static function fromString($dateString, $userTime = false) {
		if (empty($dateString)) {
			return false;
		}

		if (is_string($dateString) || get_class($dateString) !== 'Zend_Date') {
			try {
				$date = new Zend_Date();

				// Going to be interpretting for the user's timezone instead.
				if ($userTime && !empty(self::$userTimezone)) {
					$date->setTimezone(self::$userTimezone->getName());
				}

				if (preg_match('/^\d{4}-\d{2}-\d{2}$/', $dateString)) {
					$date->set($dateString, 'yyyy-MM-dd');
				} else if (!is_numeric($dateString)) {
					$date->set($dateString);
				} else {
					$date->set($dateString, Zend_Date::TIMESTAMP);
				}

				// Set it back to server time.
				if ($userTime) {
					$date->setTimezone(self::$default);
				}
			} catch (Zend_Date_Exception $e) {
				return false;
			}
		} else {
			$date = $dateString;
		}

		return $date;
	}

	/**
	 * Returns a nicely formatted date string for given Datetime string. See
	 * http://php.net/manual/en/function.strftime.php for information on
	 * formatting using locale strings.
	 *
	 * @param string $dateString
	 *        	Datetime string or Unix timestamp
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @param string $format
	 *        	The format to use. If null, `TimeHelper::$niceFormat` is used
	 * @return string Formatted date string
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#formatting
	 * @since 1.0
	 */
	public static function nice($dateString = null, $userTime = true, $format = null) {
		if ($dateString != null) {
			$date = self::fromString($dateString, $userTime);
		} else {
			$date = new Zend_Date();
		}
		if (!$format) {
			$format = self::$niceFormat;
		}

		$format = self::convertSpecifiers($format, $date);
		return self::_format($format, $date);
	}

	/**
	 * Returns a formatted descriptive date string for given datetime string. If
	 * the given date is today, the returned string could be "Today, 16:54". If
	 * the given date was yesterday, the returned string could be "Yesterday,
	 * 16:54". If $dateString's year is the current year, the returned string
	 * does not include mention of the year.
	 *
	 * @param string $dateString
	 *        	Datetime string or Unix timestamp
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return string Described, relative date string
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#formatting
	 * @since 1.0
	 */
	public static function niceShort($dateString = null, $userTime = true) {
		$date = $dateString ? self::fromString($dateString, $userTime) : time();

		if (self::isToday($dateString, $userTime)) {
			$ret = __d('cake', 'Today, %s', self::_format(Zend_Date::TIME_SHORT, $date, $userTime));
		} elseif (self::wasYesterday($dateString, $userTime)) {
			$ret = __d('cake', 'Yesterday, %s',
					self::_format(Zend_Date::TIME_SHORT, $date, $userTime));
		} else {
			$ret = self::_format(Zend_Date::DATETIME_FULL, $date, $userTime);
		}

		return $ret;
	}

	/**
	 * Returns true if given datetime string is today.
	 *
	 * @param string $dateString
	 *        	Datetime string or Unix timestamp
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return boolean True if datetime string is today
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#testing-time
	 * @since 1.0
	 */
	public static function isToday($dateString, $userTime = true) {
		$date = self::fromString($dateString, !$userTime);

		if ($userTime && !empty(self::$userTimezone)) {
			$date->setTimezone(self::$userTimezone->getName());
		}

		return $date->isToday();
	}

	/**
	 * Returns true if given datetime string is within this week.
	 *
	 * @param string $dateString
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return boolean True if datetime string is within current week
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#testing-time
	 * @since 1.0
	 */
	public static function isThisWeek($dateString, $userTime = true) {
		$date = self::fromString($dateString, !$userTime);
		$today = Zend_Date::now();

		if ($userTime && !empty(self::$userTimezone)) {
			$date->setTimezone(self::$userTimezone->getName());
			$today->setTimezone(self::$userTimezone->getName());
		}

		return $date->toString('ww YYYY') == $today->toString('ww YYYY');
	}

	/**
	 * Returns true if given datetime string is within this month
	 *
	 * @param string $dateString
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return boolean True if datetime string is within current month
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#testing-time
	 * @since 1.0
	 */
	public static function isThisMonth($dateString, $userTime = true) {
		$date = self::fromString($dateString, !$userTime);
		$today = Zend_Date::now();

		if ($userTime && !empty(self::$userTimezone)) {
			$date->setTimezone(self::$userTimezone->getName());
			$today->setTimezone(self::$userTimezone->getName());
		}

		return $date->toString('MM YYYY') == $today->toString('MM YYYY');
	}

	/**
	 * Returns true if given datetime string is within current year.
	 *
	 * @param string $dateString
	 *        	Datetime string or Unix timestamp
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return boolean True if datetime string is within current year
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#testing-time
	 * @since 1.0
	 */
	public static function isThisYear($dateString, $userTime = true) {
		$date = self::fromString($dateString, !$userTime);
		$today = Zend_Date::now();

		if ($userTime && !empty(self::$userTimezone)) {
			$date->setTimezone(self::$userTimezone->getName());
			$today->setTimezone(self::$userTimezone->getName());
		}

		return $date->getYear() == $today->getYear();
	}

	/**
	 * Returns true if given datetime string was yesterday.
	 *
	 * @param string $dateString
	 *        	Datetime string or Unix timestamp
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return boolean True if datetime string was yesterday
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#testing-time
	 * @since 1.0
	 */
	public static function wasYesterday($dateString, $userTime = true) {
		$date = self::fromString($dateString, !$userTime);
		if ($userTime && !empty(self::$userTimezone)) {
			$date->setTimezone(self::$userTimezone->getName());
		}

		return $date->isYesterday();
	}

	/**
	 * Returns true if given datetime string is tomorrow.
	 *
	 * @param string $dateString
	 *        	Datetime string or Unix timestamp
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return boolean True if datetime string was yesterday
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#testing-time
	 * @since 1.0
	 */
	public static function isTomorrow($dateString, $userTime = true) {
		$date = self::fromString($dateString, !$userTime);
		if ($userTime && !empty(self::$userTimezone)) {
			$date->setTimezone(self::$userTimezone->getName());
		}

		return $date->isTomorrow();
	}

	/**
	 * Returns the quarter
	 *
	 * @param string $dateString
	 * @param boolean $range
	 *        	if true returns a range in Y-m-d format
	 * @return mixed 1, 2, 3, or 4 quarter of year or array if $range true
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#formatting
	 * @since 1.0
	 */
	public static function toQuarter($dateString, $range = false) {
		$time = self::fromString($dateString);
		$date = ceil($time->toString('M') / 3);

		if ($range === true) {
			$range = 'YYYY-MM-dd';
		}

		if ($range !== false) {
			$year = $time->toString('YYYY');

			switch ($date) {
				case 1:
					$date = array($year . '-01-01', $year . '-03-31');
					break;
				case 2:
					$date = array($year . '-04-01', $year . '-06-30');
					break;
				case 3:
					$date = array($year . '-07-01', $year . '-09-30');
					break;
				case 4:
					$date = array($year . '-10-01', $year . '-12-31');
					break;
			}
		}
		return $date;
	}

	/**
	 * Returns a UNIX timestamp from a textual datetime description. Wrapper for
	 * PHP function strtotime().
	 *
	 * @param string $dateString
	 *        	Datetime string to be represented as a Unix timestamp
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return integer Unix timestamp
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#formatting
	 * @since 1.0
	 */
	public static function toUnix($dateString, $userTime = true) {
		$date = self::fromString($dateString, !$userTime);
		if ($userTime && !empty(self::$userTimezone)) {
			$date->setTimezone(self::$userTimezone->getName());
		}
		return $date->getTimestamp();
	}

	/**
	 * Returns a date formatted for Atom RSS feeds.
	 *
	 * @param string $dateString
	 *        	Datetime string or Unix timestamp
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return string Formatted date string
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#formatting
	 * @since 1.0
	 */
	public static function toAtom($dateString, $userTime = true) {
		$date = self::fromString($dateString, !$userTime);

		if ($userTime && !empty(self::$userTimezone)) {
			$date->setTimezone(self::$userTimezone->getName());
		}

		return $date->toString(Zend_Date::ATOM);
	}

	/**
	 * Formats date for RSS feeds
	 *
	 * @param string $dateString
	 *        	Datetime string or Unix timestamp
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return string Formatted date string
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#formatting
	 * @since 1.0
	 */
	public static function toRSS($dateString, $userTime = true) {
		$date = self::fromString($dateString, false);

		if ($userTime && !empty(self::$userTimezone)) {
			$date->setTimezone(self::$userTimezone->getName());
		}

		return $date->toString(Zend_Date::RSS);
	}

	/**
	 * Returns either a relative date or a formatted date depending on the
	 * difference between the current time and given datetime. $datetime should
	 * be in a <i>strtotime</i> - parsable format, like MySQL's datetime
	 * datatype. ### Options: - `format` => a fall back format if the relative
	 * time is longer than the duration specified by end - `end` => The end of
	 * relative time telling - `userOffset` => Users offset from GMT (in hours)
	 * Relative dates look something like this: 3 weeks, 4 days ago 15 seconds
	 * ago Default date formatting is d/m/yy e.g: on 18/2/09 The returned string
	 * includes 'ago' or 'on' and assumes you'll properly add a word like
	 * 'Posted ' before the function output.
	 *
	 * @param string $dateTime
	 *        	Datetime string or Unix timestamp
	 * @param array $options
	 *        	Default format if timestamp is used in $dateString
	 * @return string Relative time string.
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#formatting
	 * @since 1.0
	 */
	public static function timeAgoInWords($dateTime, $options = array()) {
		$userTime = true;
		if (is_array($options) && isset($options['userTime'])) {
			$userTime = $options['userTime'];
		}
		$now = Zend_Date::now();
		$endTime = Zend_Date::now();
		$dateTime = self::fromString($dateTime, !$userTime);
		if ($userTime && !empty(self::$userTimezone)) {
			$now->setTimezone(self::$userTimezone->getName());
			$endDate->setTimezone(self::$userTimezone->getName());
			$dateTime->setTimezone(self::$userTimezone->getName());
		}

		$inSeconds = $dateTime->getTimestamp();
		$backwards = ($inSeconds > $now->getTimestamp());

		$format = Zend_Date::DATE_SHORT;
		$end = '+1 month';

		if (is_array($options)) {
			if (isset($options['format'])) {
				$format = $options['format'];
				unset($options['format']);
			}
			if (isset($options['end'])) {
				$end = $options['end'];
				unset($options['end']);
			}
		} else {
			$format = $options;
		}

		if ($backwards) {
			$futureTime = $inSeconds;
			$pastTime = $now->getTimestamp();
		} else {
			$futureTime = $now->getTimestamp();
			$pastTime = $inSeconds;
		}
		$diff = $futureTime - $pastTime;

		// If more than a week, then take into account the length of months
		if ($diff >= 604800) {
			list ( $future['H'], $future['i'], $future['s'], $future['d'], $future['m'], $future['Y'] ) = explode(
					'/', date('H/i/s/d/m/Y', $futureTime));

			list ( $past['H'], $past['i'], $past['s'], $past['d'], $past['m'], $past['Y'] ) = explode(
					'/', date('H/i/s/d/m/Y', $pastTime));
			$years = $months = $weeks = $days = $hours = $minutes = $seconds = 0;

			if ($future['Y'] == $past['Y'] && $future['m'] == $past['m']) {
				$months = 0;
				$years = 0;
			} else {
				if ($future['Y'] == $past['Y']) {
					$months = $future['m'] - $past['m'];
				} else {
					$years = $future['Y'] - $past['Y'];
					$months = $future['m'] + ((12 * $years) - $past['m']);

					if ($months >= 12) {
						$years = floor($months / 12);
						$months = $months - ($years * 12);
					}

					if ($future['m'] < $past['m'] && $future['Y'] - $past['Y'] == 1) {
						$years--;
					}
				}
			}

			if ($future['d'] >= $past['d']) {
				$days = $future['d'] - $past['d'];
			} else {
				$daysInPastMonth = date('t', $pastTime);
				$daysInFutureMonth = date('t', mktime(0, 0, 0, $future['m'] - 1, 1, $future['Y']));

				if (!$backwards) {
					$days = ($daysInPastMonth - $past['d']) + $future['d'];
				} else {
					$days = ($daysInFutureMonth - $past['d']) + $future['d'];
				}

				if ($future['m'] != $past['m']) {
					$months--;
				}
			}

			if ($months == 0 && $years >= 1 && $diff < ($years * 31536000)) {
				$months = 11;
				$years--;
			}

			if ($months >= 12) {
				$years = $years + 1;
				$months = $months - 12;
			}

			if ($days >= 7) {
				$weeks = floor($days / 7);
				$days = $days - ($weeks * 7);
			}
		} else {
			$years = $months = $weeks = 0;
			$days = floor($diff / 86400);

			$diff = $diff - ($days * 86400);

			$hours = floor($diff / 3600);
			$diff = $diff - ($hours * 3600);

			$minutes = floor($diff / 60);
			$diff = $diff - ($minutes * 60);
			$seconds = $diff;
		}
		$relativeDate = '';
		$diff = $futureTime - $pastTime;

		$endDate->add($end);

		if ($diff > abs($now->getTimestamp() - $end->getTimestamp())) {
			$relativeDate = __d('cake', 'on %s', $dateTime->toString($format));
		} else {
			if ($years > 0) {
				// years and months and days
				$relativeDate .= ($relativeDate ? ', ' : '') .
						 __dn('cake', '%d year', '%d years', $years, $years);
				$relativeDate .= $months > 0 ? ($relativeDate ? ', ' : '') .
						 __dn('cake', '%d month', '%d months', $months, $months) : '';
				$relativeDate .= $weeks > 0 ? ($relativeDate ? ', ' : '') .
						 __dn('cake', '%d week', '%d weeks', $weeks, $weeks) : '';
				$relativeDate .= $days > 0 ? ($relativeDate ? ', ' : '') .
						 __dn('cake', '%d day', '%d days', $days, $days) : '';
			} elseif (abs($months) > 0) {
				// months, weeks and days
				$relativeDate .= ($relativeDate ? ', ' : '') .
						 __dn('cake', '%d month', '%d months', $months, $months);
				$relativeDate .= $weeks > 0 ? ($relativeDate ? ', ' : '') .
						 __dn('cake', '%d week', '%d weeks', $weeks, $weeks) : '';
				$relativeDate .= $days > 0 ? ($relativeDate ? ', ' : '') .
						 __dn('cake', '%d day', '%d days', $days, $days) : '';
			} elseif (abs($weeks) > 0) {
				// weeks and days
				$relativeDate .= ($relativeDate ? ', ' : '') .
						 __dn('cake', '%d week', '%d weeks', $weeks, $weeks);
				$relativeDate .= $days > 0 ? ($relativeDate ? ', ' : '') .
						 __dn('cake', '%d day', '%d days', $days, $days) : '';
			} elseif (abs($days) > 0) {
				// days and hours
				$relativeDate .= ($relativeDate ? ', ' : '') .
						 __dn('cake', '%d day', '%d days', $days, $days);
				$relativeDate .= $hours > 0 ? ($relativeDate ? ', ' : '') .
						 __dn('cake', '%d hour', '%d hours', $hours, $hours) : '';
			} elseif (abs($hours) > 0) {
				// hours and minutes
				$relativeDate .= ($relativeDate ? ', ' : '') .
						 __dn('cake', '%d hour', '%d hours', $hours, $hours);
				$relativeDate .= $minutes > 0 ? ($relativeDate ? ', ' : '') .
						 __dn('cake', '%d minute', '%d minutes', $minutes, $minutes) : '';
			} elseif (abs($minutes) > 0) {
				// minutes only
				$relativeDate .= ($relativeDate ? ', ' : '') .
						 __dn('cake', '%d minute', '%d minutes', $minutes, $minutes);
			} else {
				// seconds only
				$relativeDate .= ($relativeDate ? ', ' : '') .
						 __dn('cake', '%d second', '%d seconds', $seconds, $seconds);
			}

			if (!$backwards) {
				$relativeDate = __d('cake', '%s ago', $relativeDate);
			}
		}
		return $relativeDate;
	}

	/**
	 * Returns true if specified datetime was within the interval specified,
	 * else false.
	 *
	 * @param mixed $timeInterval
	 *        	the numeric value with space then time type. Example of valid
	 *        	types: 6 hours, 2 days, 1 minute.
	 * @param mixed $dateString
	 *        	the datestring or unix timestamp to compare
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return boolean
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#testing-time
	 * @since 1.0
	 */
	public static function wasWithinLast($timeInterval, $dateString, $userTime = true) {
		$tmp = str_replace(' ', '', $timeInterval);
		if (is_numeric($tmp)) {
			$timeInterval = $tmp . ' ' . __d('cake', 'days');
		}

		$date = self::fromString($dateString, !$userTime);
		$interval = self::fromString('-' . $timeInterval);

		if ($date >= $interval && $date <= time()) {
			return true;
		}

		return false;
	}

	/**
	 * Returns gmt as a UNIX timestamp.
	 *
	 * @param string $string
	 *        	UNIX timestamp or a valid strtotime() date string
	 * @return integer UNIX timestamp
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#formatting
	 * @since 1.0
	 */
	public static function gmt($string = null) {
		if ($string != null) {
			$string = self::fromString($string);
		} else {
			$string = time();
		}

		$hour = intval(date("G", $string));
		$minute = intval(date("i", $string));
		$second = intval(date("s", $string));
		$month = intval(date("n", $string));
		$day = intval(date("j", $string));
		$year = intval(date("Y", $string));

		return gmmktime($hour, $minute, $second, $month, $day, $year);
	}

	/**
	 * Returns a formatted date string, given either a UNIX timestamp or a valid
	 * strtotime() date string. This function also accepts a time string and a
	 * format string as first and second parameters. In that case this function
	 * behaves as a wrapper for TimeHelper::i18nFormat()
	 *
	 * @param string $format
	 *        	date format string (or a DateTime string)
	 * @param string $date
	 *        	Datetime string (or a date format string)
	 * @param boolean $invalid
	 *        	flag to ignore results of fromString == false
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return string Formatted date string
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#formatting
	 * @since 1.0
	 */
	public static function format($format, $date = null, $invalid = false, $userTime = true) {
		$time = self::fromString($date, $userTime);
		$_time = self::fromString($format, $userTime);

		if (is_numeric($_time) && $time === false) {
			$format = $date;
			return self::i18nFormat($_time, $format, $invalid, $userTime);
		}
		if ($time === false && $invalid !== false) {
			return $invalid;
		}

		return self::_format($format, $time, $userTime);
	}

	/**
	 * Returns a formatted date string, given either a UNIX timestamp or a valid
	 * strtotime() date string. It take in account the default date format for
	 * the current language if a LC_TIME file is used.
	 *
	 * @param string $date
	 *        	Datetime string
	 * @param string $format
	 *        	strftime format string.
	 * @param boolean $invalid
	 *        	flag to ignore results of fromString == false
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return string Formatted and translated date string
	 * @link
	 *       http://book.cakephp.org/2.0/en/core-libraries/helpers/time.html#formatting
	 * @since 1.0
	 */
	public static function i18nFormat($date, $format = null, $invalid = false, $userTime = true) {
		$date = self::fromString($date, !$userTime);
		if ($date === false && $invalid !== false) {
			return $invalid;
		}
		if (empty($format)) {
			$format = Zend_Date::DATE_SHORT;
		}
		$format = self::convertSpecifiers($format, $date);
		return self::_format($format, $date, $userTime);
	}

	/**
	 * Multibyte wrapper for strftime. Handles utf8_encoding the result of
	 * strftime when necessary.
	 *
	 * @param string $format
	 *        	Format string.
	 * @param int $date
	 *        	Timestamp to format.
	 * @param boolean $userTime
	 *        	If false, convert from user's timezone to server's timezone.
	 *        	If true, convert to user's timezone.
	 * @return string formatted string with correct encoding.
	 * @since 1.0
	 */
	protected static function _format($format, $date, $userTime = true) {
		if ($userTime && !empty(self::$userTimezone)) {
			$date->setTimezone(self::$userTimezone->getName());
		}

		$format = $date->toString($format);

		$encoding = Configure::read('App.encoding');

		if (!empty($encoding) && $encoding === 'UTF-8') {
			if (function_exists('mb_check_encoding')) {
				$valid = mb_check_encoding($format, $encoding);
			} else {
				$valid = !Multibyte::checkMultibyte($format);
			}
			if (!$valid) {
				$format = utf8_encode($format);
			}
		}
		return $format;
	}
}